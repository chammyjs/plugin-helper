# @chammy/plugin-helper Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

---

## [0.2.0]
### Added
* Add changelog.
* Add MIT license file.
* [#9] Official support for Windows.
* [#11] Add `Question#getPossilbeAnswers` method.
* [#11] Add `EDITOR` type to `AnswerType`.
* [#14] Add support for Node 12.
* [#13] Add `PluginStage` to support ordering plugins into stages.
* [#15] Expose `AnswerType` as a named export of the package.

### Removed
* [#14] **BREAKING CHANGE**: Remove support for Node 6.
* [#15] **BREAKING CHANGE**: Remove `Question.AnswerType` static property.

## 0.1.0 – 2017-12-11
### Added
* First working version, yay!

[#9]: https://gitlab.com/chammyjs/plugin-helper/issues/9
[#11]: https://gitlab.com/chammyjs/plugin-helper/issues/11
[#13]: https://gitlab.com/chammyjs/plugin-helper/issues/13
[#14]: https://gitlab.com/chammyjs/plugin-helper/issues/14
[#15]: https://gitlab.com/chammyjs/plugin-helper/issues/15

[0.2.0]: https://gitlab.com/chammyjs/plugin-helper/compare/v0.1.0...v0.2.0
