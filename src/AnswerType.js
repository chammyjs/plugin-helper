import { Enum } from 'enumify';

/**
 * Represents types of answer
 */
class AnswerType extends Enum {}

/**
 * Available types:
 * - STRING
 * - NUMBER
 * - BOOLEAN
 * - ARRAY
 * - OBJECT
 * - EDITOR
 */
AnswerType.initEnum( [ 'STRING', 'NUMBER', 	'BOOLEAN', 'ARRAY', 'OBJECT', 'EDITOR' ] );

export default AnswerType;
