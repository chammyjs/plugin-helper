import { Enum } from 'enumify';

/**
 * Represents stage, in which plugin will be executed.
 *
 * Order of stages:
 * 1. PREINIT – intended for bootstraping the project (e.g. creating `package.json` file);
 * 2. INIT – intended for tasks that should be executed right after bootstrap and before other tasks;
 * 3. CONTENT – most of the tasks wil be executed here;
 * 4. DEPLOY – intended for tasks that should be executed at the end (e.g. pushing to git repository).
 */
class PluginStage extends Enum {}

PluginStage.initEnum( [
	'PREINIT',
	'INIT',
	'CONTENT',
	'DEPLOY'
] );

export default PluginStage;
