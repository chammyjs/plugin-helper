import { expect } from 'chai';
import * as pluginHelper from '../src/index.js';
import Plugin from '../src/Plugin.js';
import Question from '../src/Question.js';
import PluginStage from '../src/PluginStage.js';
import AnswerType from '../src/AnswerType.js';

describe( 'pluginHelper', () => {

	it( 'exists', () => {
		expect( pluginHelper ).to.exist;
	} );

	it( 'exposes Question, Plugin, PluginStage and AnswerType', () => {
		expect( pluginHelper ).to.have.all.keys( 'Question', 'Plugin', 'PluginStage', 'AnswerType' );
	} );

	describe( 'Question', () => {
		it( 'is a Question class', () => {
			expect( pluginHelper.Question ).to.equal( Question );
		} );
	} );

	describe( 'Plugin', () => {
		it( 'is a Plugin class', () => {
			expect( pluginHelper.Plugin ).to.equal( Plugin );
		} );
	} );

	describe( 'PluginStage', () => {
		it( 'is a PluginStage class', () => {
			expect( pluginHelper.PluginStage ).to.equal( PluginStage );
		} );
	} );

	describe( 'AnswerType', () => {
		it( 'is a AnswerType class', () => {
			expect( pluginHelper.AnswerType ).to.equal( AnswerType );
		} );
	} );
} );
