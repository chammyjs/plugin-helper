import { expect } from 'chai';
import PluginStage from '../src/PluginStage.js';

describe( 'PluginStage', () => {
	it( 'exists', () => {
		expect( PluginStage ).to.exist;
	} );

	it( 'has property PREINIT', () => {
		expect( PluginStage.enumValues[ 0 ].name ).to.equal( 'PREINIT' );
	} );

	it( 'has property INIT', () => {
		expect( PluginStage.enumValues[ 1 ].name ).to.equal( 'INIT' );
	} );

	it( 'has property CONTENT', () => {
		expect( PluginStage.enumValues[ 2 ].name ).to.equal( 'CONTENT' );
	} );

	it( 'has property DEPLOY', () => {
		expect( PluginStage.enumValues[ 3 ].name ).to.equal( 'DEPLOY' );
	} );
} );
