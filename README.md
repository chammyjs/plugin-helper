# @chammy/plugin-helper

[![pipeline status](https://gitlab.com/chammyjs/plugin-helper/badges/master/pipeline.svg)](https://gitlab.com/chammyjs/plugin-helper/commits/master)
[![coverage report](https://gitlab.com/chammyjs/plugin-helper/badges/master/coverage.svg)](https://gitlab.com/chammyjs/plugin-helper/commits/master)
[![npm (scoped)](https://img.shields.io/npm/v/@chammy/plugin-helper.svg)](https://npmjs.com/package/@chammy/plugin-helper)
